#!/bin/bash

declare -a mangadex_groups
MANGADEX_GROUPS="$HOME/.mangadex-groups.txt"
if [[ -z "$TMPDIR" ]]; then
	TMPDIR='/tmp'
fi
declare -l mangadex_domain="mangadex.org"
_mangadex_groups=0
mangadex_groups=( 'null' )

__mangadex_groups_setup () {
	_mangadex_groups=1
	[ -f "$MANGADEX_GROUPS" ] \
	&& cat "$MANGADEX_GROUPS" \
	| while IFS=';' read -r id group; do
		mangadex_groups[$id]="$group"
	done
}

declare -A mangadex_languages=( [sa]='Arabic' [bd]='Bengali' [bg]='Bulgarian' [mm]='Burmese' [ct]='Catalan' [cn]='Chinese (Simp)' [hk]='Chinese (Trad)' [cz]='Czech' [dk]='Danish' [nl]='Dutch' [gb]='English' [ph]='Filipino' [fi]='Finnish' [fr]='French' [de]='German' [gr]='Greek' [hu]='Hungarian' [id]='Indonesian' [it]='Italian' [jp]='Japanese' [kr]='Korean' [my]='Malay' [mn]='Mongolian' [ir]='Persian' [pl]='Polish' [br]='Portuguese (Br)' [pt]='Portuguese (Pt)' [ro]='Romanian' [ru]='Russian' [rs]='Serbo-Croatian' [es]='Spanish (Es)' [mx]='Spanish (LATAM)' [se]='Swedish' [th]='Thai' [tr]='Turkish' [ua]='Ukrainian' [vn]='Vietnamese' )

function __zws {
	local v="$@" n=0
	echo -ne $@
	declare -i n="${#v[*]}"
	for (( ; n--; )); do echo -ne '\b'; done
}

function __mangadex-curl {
	>&2 __zws 'Waiting on network'
	curl \
		--doh-url https://cloudflare-dns.com/dns-query \
		-c ~/.cache/mangadex-cookies \
		-c ~/.cache/mangadex-cookies \
		--http2 \
		--compressed \
		--parallel \
		-# \
		$@
	return $?
}
# parameters: id, name. 
function __set_mangadex_group_id {
	local id
	declare -i id
	local name
	id="$1"
	name="$2"
	if [[ -z "${mangadex_groups[$id]}" ]]; then
		mangadex_groups[$id]="$name"
		echo "$id;$name" >> "$MANGADEX_GROUPS"
		echo "mangadex_groups[$id]='$name'"
	fi
}
# parameters: JSON(manga)
function get_mangadex_group_ids {
	local mdx_group_ids
	declare -i mdx_group_ids
	local manga="${1:-"\{\}"}"
	echo 'indexing group ids'
	jq -cr "
	.chapter[]? | 
		[.group_id, .group_name],
		if .group_id_2 != 0 then [.group_id_2, .group_name_2] else null end,
		if .group_id_3 != 0 then [.group_id_3, .group_name_3] else null end
	| arrays
	" <<< "$manga" | uniq -w 9 | while read -r group; do
		__set_mangadex_group_id "$(jq -r ".[0]" <<< "$group")" "$(jq -r ".[1]" <<< "$group")";
	done
	if [ $? != 0 ]; then return $?; fi
	echo 'completed indexing group ids'
}


function mangadex-dl-chapter-single {
	[ $_mangadex_groups == 0 ] && __mangadex_groups_setup
	local mtitle ctitle _ctitle file ext filename n _vol _ch _ct
	local tmpfs=`mktemp -d`
	local currfs=`pwd`
	mkdir -p "$tmpfs"

	echo "getting chapter $1"
	local mch=`__mangadex-curl "https://$mangadex_domain/api/chapter/$(basename "$1")?server=${server:0}"`

	local dataurl=`jq '.hash' -r <<< "$mch"`
	local srv=`jq '.server' -r <<< "$mch"`
	local page_array=`jq '.page_array[]' -r <<< "$mch"`
	if [[ "$srv" == '/data/' ]]; then
		srv="https://$mangadex_domain/data/"
	fi

	local _vol=`jq '.volume' -r <<< "$mch"`
	if [[ -n "$_vol" ]]; then
		_ct="Vol. $(printf '%03d' "$_vol") "
	fi
	local _ch=`jq '.chapter' -r <<< "$mch"`
	if [[ -n "$_ch" ]]; then
		_ct="${_ct}Ch. $(printf '%07.2f' "$_ch") "
	fi
	if [[ -n "$_ct" ]]; then
		_ct="$_ct- "
	fi


	if [[ -z "$2" ]]; then
		local chm
		echo "getting manga title $(jq '.manga_id' -r <<< "$mch")"
		chm=`__mangadex-curl "https://$mangadex_domain/api/manga/$(jq '.manga_id' -r <<< "$mch")"`
		mtitle=`jq '.manga.title' -r <<< "$chm"`
		get_mangadex_group_ids "$chm"
	else
		mtitle="$2"
	fi
	echo "manga title: $mtitle"


	local groups group_id
	declare -i group_id
	group_id=`jq -r ".group_id" <<< "$mch"`
	groups="${mangadex_groups[$group_id]}"

	group_id=`jq -r ".group_id_2" <<< "$mch"`
	if (( $group_id > 0 )); then
		groups="$groups, ${mangadex_groups[$group_id]}"
	fi
	group_id=`jq -r ".group_id_3" <<< "$mch"`
	if (( $group_id > 0 )); then
		groups="$groups, ${mangadex_groups[$group_id]}"
	fi
	_ctitle=`jq '.title' -r <<< "$mch"`
	if [[ "$_ctitle" != '' ]]; then
		_ctitle="$_ctitle "
	fi
	ctitle="(${mangadex_languages[$(jq '.lang_code' -r <<< "$mch")]}) $_ct$_ctitle[$groups]"
	echo "chapter title: $ctitle"

	local files full_filenames
	declare -a files=( ) full_filenames=( )
	declare -i nfiles=0
	>&2 __zws "srv is $srv, dataurl is $dataurl\n"
	for i in $page_array; do
		ext="${i##*.}"
		n=`basename "$i" .$ext`
		filename=`printf %09d.$ext "${n//[a-zA-Z]/}"`
		files+=("-o");
		files+=("$tmpfs/$filename");
		files+=("$srv$dataurl/$i");
		full_filenames+=("$tmpfs/$filename");
		(( nfiles += 1 ));
	done

	if (( nfiles == 1 )); then
		echo "Downloading 1 file"
	else
		echo "Downloading $nfiles files"
	fi
	__mangadex-curl --parallel ${files[@]}
	local mdir
	mdir="${mtitle//\//_}"
	mkdir -p "$currfs/$mdir"
	local cdir
	cdir="$currfs/$mdir/${ctitle//\//_}"
	
	local saveas
	declare -l saveas
	saveas="$3"
	if [[ "$saveas" == '' ]]; then
		if [[ "$SAVEAS" == '' ]]; then
			saveas="dir"
		else
			saveas="$SAVEAS"
		fi
	fi
	echo "\$SAVEAS == $SAVEAS == $saveas"
	case "$saveas" in
		zip | cbz)
			zip --junk-paths -9 "$cdir.$saveas" ${full_filenames[@]}
			echo "$tmpfs/* -> $cdir.$saveas"
			rm ${full_filenames[@]}
			rmdir $tmpfs
			;;
		tar | cbt)
			tar cf "$cdir.$saveas" --sort name --directory "$tmpfs" .
			echo "$tmpfs/* -> $cdir.$saveas"
			rm ${full_filenames[@]}
			rmdir "$tmpfs"
			;;
		*)
			;&
		mv | folder | dir)
			mkdir -p "$cdir"
			mv ${full_filenames[@]} -t "$cdir/"
			rmdir "$tmpfs"
			echo "$tmpfs -> $cdir"
			;;
	esac
}
function mangadex-dl-chapters {
	[ $_mangadex_groups == 0 ] && __mangadex_groups_setup || echo
	for id in "$@"; do
		mangadex-dl-chapter-single "$(basename "$id")" '' "$SAVEAS";
	done
}
function mangadex-dl-manga {
	[ $_mangadex_groups == 0 ] && __mangadex_groups_setup
	local manga mtitle id tmpa tmpv
	declare -i id
	declare -ia tmpa=()
	for tmpv in $@; do tmpa+=( "$(basename "$tmpv")" ); done
	for id in ${tmpa[@]}; do
		echo "getting manga title $id"
		manga=`__mangadex-curl "https://$mangadex_domain/api/?id=$id&type=manga&server=na2"`
		mtitle=`jq -r '.manga.title' <<< "$manga"`
		time get_mangadex_group_ids "$manga"
		if [ $? != 0 ]; then return $?; fi
		echo "manga title recieved: $mtitle"
		if [[ "$LANG_CODE" != '' ]]; then
			jq -r ".chapter | to_entries | map(select(.value.lang_code==\"$LANG_CODE\")) | group_by(if .value.volume != \"\" then .value.volume else infinite end | tonumber) | sort_by(if .[0].value.volume != \"\" then .[0].value.volume else infinite end | tonumber)[][].key" <<< "$manga" | while read -r id; do
				mangadex-dl-chapter-single \
					"$id" \
					"$mtitle" \
					"$SAVEAS"
				if [ $? != 0 ]; then return $?; fi
			done
		else
			jq -r ".chapter | to_entries | group_by(if .value.volume != \"\" then .value.volume else infinite end | tonumber) | sort_by(if .[0].value.volume != \"\" then .[0].value.volume else infinite end | tonumber)[][].key" <<< "$manga" | while read -r id; do
				mangadex-dl-chapter-single \
					"$id" \
					"$mtitle" \
					"$SAVEAS"
			done
		fi
	done
}
